FROM php:8.0-apache
RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli
RUN apt-get update && apt-get upgrade -y
#ADD ./php-mysql/users.sql /docker-entrypoint-initdb.d
WORKDIR /var/www/html/
COPY ./apps .
EXPOSE 80
# # # VOLUME [ "./mydata" ]
#CMD [ "php", "./index.php"]

# FROM mysql:latest
# ENV MYSQL_ROOT_PASSWORD Athena.s0226
# ENV MYSQL_DATABASE sample_db
# ADD ./php-mysql/users.sql /docker-entrypoint-initdb.d
# EXPOSE 3300